//
//  ViewController.swift
//  itunesaffiliatedemo
//
//  Created by JASON FONG (MBC-ISD-OOCLL/HKG) on 22/8/2018.
//  Copyright © 2018 JASON FONG (MBC-ISD-OOCLL/HKG). All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

enum ViewStatus{
    case idle
    case typing
    case searching
    case result
}

class ViewController: UIViewController, UISearchBarDelegate, UISearchControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    //Search bar
    private var searchController:UISearchController!
    
    //Collection View
    private var collectionViewData:[iTunesTrackItem] = []
    @IBOutlet weak private var resultCollectionView: UICollectionView!
    
    //View And status
    private var _status:ViewStatus! = .idle
    var status:ViewStatus {
        get{
            return _status
        }
        set{
            _status = newValue
            
            //Sync UI with Status
            SyncUIWithStatus()
        }
    }
    @IBOutlet weak private var emptyResultView: UIView!
    @IBOutlet weak private var loadingView: UIView!
    @IBOutlet weak private var searchHints: UIView!
    @IBOutlet weak private var placeHolderView: UIView!
    
    
    //Music Player Bar
    private var playerBarController:MusicPlayerBarViewController?
    @IBOutlet weak private var playerBarBottom: NSLayoutConstraint!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Create the result controller
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        //Config navigation bar
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        
        //For searchController
        self.definesPresentationContext = true
        
        
        //init the status
        status = .idle
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Register Notification for play request
        NotificationCenter.default.addObserver(self, selector: #selector(didStartPlayRequest(notification:)), name: MusicPlayerBarViewController.PlayerDidStartNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didStopPlayRequest(notification:)), name: MusicPlayerBarViewController.PlayerDidStopNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        //Unregister Notification
        NotificationCenter.default.removeObserver(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoadMusicPlayerBar"{
            playerBarController = segue.destination as? MusicPlayerBarViewController
        }
    }
    
    //MARK: - Notification Handle
    @objc private func didStartPlayRequest(notification: NSNotification) {
        
        //Update the music player bar position
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 0.0,
                       options: .beginFromCurrentState,
                       animations: {
                        self.playerBarBottom.constant = self.view.safeAreaInsets.bottom + 24
                        self.view.layoutIfNeeded()
        },
                       completion: nil)
        
        //Reload Collection View to reflect the current playing item
        resultCollectionView.reloadData()
    }
    
    @objc private func didStopPlayRequest(notification: NSNotification) {
        
        //Update the music player bar position
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 0.0,
                       options: .beginFromCurrentState,
                       animations: {
                        self.playerBarBottom.constant = -80
                        self.view.layoutIfNeeded()
        },
                       completion: nil)
        
        //Reload Collection View to reflect the current playing item
        resultCollectionView.reloadData()
    }
    
    
    //MARK: - UI
    private func SyncUIWithStatus(){
        
        switch status {
        case .idle:
            
            //hide views
            searchHints.isHidden = true
            loadingView.isHidden = true
            emptyResultView.isHidden = true
            
            //check if no previous result
            if collectionViewData.count == 0{
                placeHolderView.isHidden = false
                resultCollectionView.isHidden = true
            }else{
                placeHolderView.isHidden = true
                resultCollectionView.isHidden = false
            }
            
        case .typing:
            
            
            //hide views
            placeHolderView.isHidden = true
            loadingView.isHidden = true
            emptyResultView.isHidden = true
            
            //check if no previous result
            if collectionViewData.count == 0{
                searchHints.isHidden = false
                resultCollectionView.isHidden = true
            }else{
                searchHints.isHidden = true
                resultCollectionView.isHidden = false

            }
            
        case .searching:
            
            //hide views
            placeHolderView.isHidden = true
            searchHints.isHidden = true
            emptyResultView.isHidden = true
            resultCollectionView.isHidden = true
            
            //Show loading
            loadingView.isHidden = false
            
            //CollectionView back to top
            resultCollectionView.setContentOffset(CGPoint(x: 0, y: 0 - view.safeAreaInsets.top), animated: false)
            
            
        case .result:
            
            //hide views
            placeHolderView.isHidden = true
            searchHints.isHidden = true
            loadingView.isHidden = true
            
            //Check if no result
            if collectionViewData.count == 0{
                resultCollectionView.isHidden = true
                emptyResultView.isHidden = false
            }else{
                resultCollectionView.isHidden = false
                emptyResultView.isHidden = true
            }
            
        }
    }
    
    //MARK: - Server Request
    private func requestSongTrack(with phase:String, complete:@escaping () -> Void){
        
        //Prepare the url
        let urlPrefix = "https://itunes.apple.com/search?term="
        let param = phase.replacingOccurrences(of: " ", with: "+").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let urlSubfix = "&entity=song"
        
        //Make request
        Alamofire.request("\(urlPrefix)\(param!)\(urlSubfix)", method: .get).responseObject(queue: DispatchQueue.global(qos: .background)) { (response:DataResponse<SearchTrackResult>) in
            
            switch response.result{
            case .success:
                
                //Update the data
                if let tracks = response.result.value{
                    self.collectionViewData = tracks.results
                }else{
                    self.collectionViewData = []
                }
                
            case .failure:
                NSLog("requestSongTrack request failed")
                
                //Update the data
                self.collectionViewData = []
            }
            
            //update the status
            DispatchQueue.main.async {
                complete()
            }
        }
        
        
    }
    
    //MARK: - UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        //update status
        self.status = .searching
        
        //Check Empty String
        var phase = ""
        if let text = searchBar.text{
            phase = text
        }
        
        //Perform searching
        requestSongTrack(with: phase) {
            
            //reload data
            self.resultCollectionView.reloadData()
            
            //update status
            self.status = .result
            
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.status = .idle
    }
    
    //MARK: - UISearchControllerDelegate
    func willPresentSearchController(_ searchController: UISearchController){
        self.status = .typing
    }
    
    func willDismissSearchController(_ searchController: UISearchController){
        self.status = .idle
    }
 
    //MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return collectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        //Dequeue the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbnailCell", for: indexPath) as! ThumbnailCell
        
        //Dequeue the infom
        let track = collectionViewData[indexPath.row]
        
        //Fill up the cell
        var isCellPlaying = false
        if let playingTrack  = playerBarController!.currentTrack{
            if playingTrack.trackId == track.trackId && playerBarController!.isPlaying{
                isCellPlaying = true
            }
        }
        cell.fillWith(track: track, isPlaying: isCellPlaying)
        
        
        return cell
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let screenWidth = UIScreen.main.bounds.size.width
        let cellSize = (screenWidth - 16 - 16 - 20) / 2
        return CGSize(width: cellSize, height: cellSize + 60)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        let bottomPadding = view.safeAreaInsets.bottom + 80 + 24
        return UIEdgeInsets(top: 20, left: 16, bottom: bottomPadding, right: 16)
    }

}




