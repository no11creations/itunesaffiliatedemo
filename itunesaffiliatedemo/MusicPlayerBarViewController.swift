//
//  MusicPlayerBarViewController.swift
//  itunesaffiliatedemo
//
//  Created by JASON FONG (MBC-ISD-OOCLL/HKG) on 22/8/2018.
//  Copyright © 2018 JASON FONG (MBC-ISD-OOCLL/HKG). All rights reserved.
//

import UIKit
import AVKit
import Kingfisher

class MusicPlayerBarViewController: UIViewController {
    
    //Notification
    public static let PlayRequestNotification = Notification.Name("no.11creations.TestApp.iTunesAffiliateDemo.play")
    public static let StopRequestNotification = Notification.Name("no.11creations.TestApp.iTunesAffiliateDemo.stop")
    public static let PlayerDidStartNotification = Notification.Name("no.11creations.TestApp.iTunesAffiliateDemo.started")
    public static let PlayerDidStopNotification = Notification.Name("no.11creations.TestApp.iTunesAffiliateDemo.stopped")
    
    //UI Elements
    @IBOutlet weak private var bgBlurView: UIVisualEffectView!
    @IBOutlet weak private var blurImage: UIImageView!
    @IBOutlet weak private var blurMask: UIImageView!
    @IBOutlet weak private var thumbnailImage: UIImageView!
    @IBOutlet weak private var trackNameLabel: UILabel!
    @IBOutlet weak private var albumLabel: UILabel!
    
    //Player
    public private(set) var isPlaying = false
    public private(set) var currentTrack:iTunesTrackItem?
    private var avplayer:AVPlayer!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Round the Corner
        bgBlurView.layer.cornerRadius = 12.0
        bgBlurView.clipsToBounds = true
        
        //Thumbnail border
        thumbnailImage.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        thumbnailImage.layer.borderWidth = 1.0
        
        //Create the AVPlayer
        avplayer = AVPlayer()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Register Notification for play request
        NotificationCenter.default.addObserver(self, selector: #selector(didRecevicePlayRequest(notification:)), name: MusicPlayerBarViewController.PlayRequestNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceviceStopRequest(notification:)), name: MusicPlayerBarViewController.StopRequestNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceviceStopRequest(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avplayer.currentItem)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        //Unregister Notification
        NotificationCenter.default.removeObserver(self)
        
    }
    
    //MARK: - Notification Handle
    @objc private func didRecevicePlayRequest(notification: NSNotification) {
        
        if let track = notification.object as? iTunesTrackItem{
            
            //Play the track
            play(iTunesTrack: track)
        }
        
    }
    
    @objc private func didReceviceStopRequest(notification: NSNotification) {
        
        //Stop Playing
        stop()
        
    }

    //MARK: - UI Handle
    @IBAction private func didTapPauseButton(_ sender: Any) {
        
        //Stop playing
        stop()
        
    }
    
    //MARK: - Player Controller
    func play(iTunesTrack track:iTunesTrackItem){
        
        //Check if it is currently playing?
        if isPlaying {
            //Stop the playback
            avplayer.pause()
        }
        
        //Store Current Track
        currentTrack = track
        
        //Fill the player bar information
        trackNameLabel.text = track.trackName
        albumLabel.text = track.collectionName
        
        thumbnailImage.image = nil
        blurImage.image = nil
        
        let img = ImageResource(downloadURL: URL(string: track.artworkUrl100)!)
        thumbnailImage.kf.setImage(with: img) { (orgImage, _, _, _) in
            
            //Blur Image Key
            let blurKey = "b" + String(track.trackId)
            
            //Check if blured image is cached?
            ImageCache.default.retrieveImage(forKey: blurKey, options: nil, completionHandler: { (cachedImage, _) in
                
                if let _ = cachedImage {
                    
                    //Set the blur image
                    self.blurImage.image = cachedImage!
                    
                }else{
                    
                    //A weak reference to self
                    weak var weakSelf = self
                    
                    DispatchQueue.global(qos: .background).async {
                        //Generate the blur image
                        let ciOrgImage = CIImage(image: orgImage!)
                        if let blurFilter = CIFilter(name: "CIGaussianBlur", parameters: ["inputRadius": 8,"inputImage":ciOrgImage!]){
                            if let blurOutput = blurFilter.outputImage{
                                
                                let context = CIContext(options: nil)
                                let cgImage = context.createCGImage(blurOutput, from: blurOutput.extent)
                                let blurImage = UIImage(cgImage: cgImage!)
                                
                                DispatchQueue.main.async {
                                    
                                    //Cache the Blured Image
                                    ImageCache.default.store(blurImage, forKey: blurKey)
                                    
                                    //Set the blur image
                                    if let _ = weakSelf{
                                        weakSelf!.blurImage.image = blurImage
                                    }
                                    
                                }
                            }
                        }
                    }
                    
                }
            })
        }
        
        
        
        //Create AVPlayerItem
        let item = AVPlayerItem(url: URL(string: track.previewUrl)!)
        
        //Set current item to play
        avplayer.replaceCurrentItem(with: item)
        
        //Play it
        avplayer.play()
        
        //Update the flag
        isPlaying = true
        
        //Post notification
        NotificationCenter.default.post(name: MusicPlayerBarViewController.PlayerDidStartNotification, object: nil, userInfo: nil)
        
    }
    
    func stop(){
        
        //check if is it playing
        if !isPlaying {
            NSLog("MusicPlayerBarViewController, nothing to stop?")
        }
        
        //Stop playback
        avplayer.pause()
        
        //Update the flag
        isPlaying = false
        
        //Post notification
        NotificationCenter.default.post(name: MusicPlayerBarViewController.PlayerDidStopNotification, object: nil, userInfo: nil)
        
    }
    
}
