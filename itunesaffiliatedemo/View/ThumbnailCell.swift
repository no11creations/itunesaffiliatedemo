//
//  ThumbnailCell.swift
//  itunesaffiliatedemo
//
//  Created by JASON FONG (MBC-ISD-OOCLL/HKG) on 22/8/2018.
//  Copyright © 2018 JASON FONG (MBC-ISD-OOCLL/HKG). All rights reserved.
//

import UIKit
import Kingfisher

class ThumbnailCell: UICollectionViewCell {
    
    @IBOutlet weak private var bgImg: UIImageView!
    @IBOutlet weak private var blurImage: UIImageView!
    @IBOutlet weak private var thumbnailImg: UIImageView!
    @IBOutlet weak private var cellTitleLabel: UILabel!
    @IBOutlet weak private var cellAlbumTitle: UILabel!
    @IBOutlet weak private var pauseButton: UIButton!
    
    public private(set) var trackItem:iTunesTrackItem?
    
    func fillWith(track item:iTunesTrackItem, isPlaying playing:Bool){
        
        //Store the cell data
        trackItem = item
        
        //Thumbnail border
        thumbnailImg.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        thumbnailImg.layer.borderWidth = 1.0
        
        //Update title labels
        cellTitleLabel.text = trackItem!.trackName
        cellAlbumTitle.text = trackItem!.collectionName
        
        //Clear the image
        thumbnailImg.image = nil
        blurImage.image = nil
        
        //Load the image from URL Async
        let img = ImageResource(downloadURL: URL(string: trackItem!.artworkUrl100)!)
        thumbnailImg.kf.setImage(with: img) { (orgImage, _, _, _) in
            
            //Blur Image Key
            let blurKey = "b" + String(self.trackItem!.trackId)
            
            //Check if blured image is cached?
            ImageCache.default.retrieveImage(forKey: blurKey, options: nil, completionHandler: { (cachedImage, _) in
                
                if let _ = cachedImage {
                    
                    //Set the blur image
                    self.blurImage.image = cachedImage!
                    
                }else{
                    
                    //A weak reference to self
                    weak var weakSelf = self
                    
                    DispatchQueue.global(qos: .background).async {
                        //Generate the blur image
                        let ciOrgImage = CIImage(image: orgImage!)
                        if let blurFilter = CIFilter(name: "CIGaussianBlur", parameters: ["inputRadius": 8,"inputImage":ciOrgImage!]){
                            if let blurOutput = blurFilter.outputImage{
                                
                                let context = CIContext(options: nil)
                                let cgImage = context.createCGImage(blurOutput, from: blurOutput.extent)
                                let blurImage = UIImage(cgImage: cgImage!)
                                
                                DispatchQueue.main.async {
                                    
                                    //Cache the Blured Image
                                    ImageCache.default.store(blurImage, forKey: blurKey)
                                    
                                    //Set the blur image
                                    if let _ = weakSelf{
                                        weakSelf!.blurImage.image = blurImage
                                    }
                                    
                                }
                            }
                        }
                    }
                    
                }
            })
        }
        
        //Sync if this album is playing
        setButtonPlaying(playing)
    }
    
    private func setButtonPlaying(_ playing:Bool){
        if playing {
            pauseButton.isSelected = true
            pauseButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        }else{
            pauseButton.isSelected = false
            pauseButton.backgroundColor = UIColor.clear
        }
    }
    
    @IBAction private func didTapPauseButton(_ sender: Any) {
        
        NSLog("didTapPauseButton")
        
        if pauseButton.isSelected{
            
            //Post Stop Request Notification
            NotificationCenter.default.post(name: MusicPlayerBarViewController.StopRequestNotification, object: nil)
            
        }else{
            
            //Post Play Request Notification
            NotificationCenter.default.post(name: MusicPlayerBarViewController.PlayRequestNotification, object: trackItem)
            
        }
        
    }
    
}
