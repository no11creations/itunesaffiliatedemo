//
//  iTunesTrackItem.swift
//  itunesaffiliatedemo
//
//  Created by JASON FONG (MBC-ISD-OOCLL/HKG) on 22/8/2018.
//  Copyright © 2018 JASON FONG (MBC-ISD-OOCLL/HKG). All rights reserved.
//

import UIKit
import ObjectMapper

class iTunesTrackItem: Mappable {
    
    var trackId:Int!
    var trackName:String!
    var artworkUrl100:String!
    var previewUrl:String!
    var collectionName:String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        let transform = TransformOf<String, String>(fromJSON: { (value: String?) -> String? in
            
            let replaced = value!.replacingOccurrences(of: "100x100", with: "300x300")
            
            return replaced
            
        }, toJSON: { (value: String?) -> String? in
            return value!
        })
        
        trackId <- map["trackId"]
        trackName <- map["trackName"]
        artworkUrl100 <- (map["artworkUrl100"], transform)
        previewUrl <- map["previewUrl"]
        collectionName <- map["collectionName"]
    }
    
}
