//
//  SearchTrackResult.swift
//  itunesaffiliatedemo
//
//  Created by JASON FONG (MBC-ISD-OOCLL/HKG) on 22/8/2018.
//  Copyright © 2018 JASON FONG (MBC-ISD-OOCLL/HKG). All rights reserved.
//

import UIKit
import ObjectMapper

class SearchTrackResult: Mappable {
    
    var resultCount:Int!
    var results:[iTunesTrackItem]!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultCount <- map["resultCount"]
        results <- map["results"]
    }
    
}
